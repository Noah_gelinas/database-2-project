/*SS stands for Super Store*/
-- CREATE TABLE Area
/*creating customers table*/
CREATE TABLE SS_Customers (
    customer_id     NUMBER(6)       GENERATED ALWAYS AS IDENTITY CONSTRAINT customer_pk PRIMARY KEY,
    firstname       VARCHAR2(30),
    lastname        VARCHAR2(30),
    email           VARCHAR2(40),
    address         VARCHAR2(50)
);
/*creating products table*/
CREATE TABLE SS_Products (
    product_id          NUMBER(6) GENERATED ALWAYS AS IDENTITY CONSTRAINT product_pk PRIMARY KEY,
    name                VARCHAR2(30),
    category            VARCHAR2(40)
);
/*creating orders table*/
CREATE TABLE SS_Orders (
    order_id        NUMBER(6)       GENERATED ALWAYS AS IDENTITY CONSTRAINT order_pk PRIMARY KEY,
    date_ordered    DATE,
    order_quantity  NUMBER(7),
    order_price     NUMBER(7),
    store_ordered   VARCHAR2(40),
    customer_id     NUMBER(6),
    product_id      NUMBER(6),
    
    CONSTRAINT FK_customer
        FOREIGN KEY(customer_id)
        REFERENCES SS_Customers(customer_id),
        
    CONSTRAINT FK_product1
        FOREIGN KEY(product_id) 
        REFERENCES SS_Products(product_id)
);
/*creating reviews table*/
CREATE TABLE SS_Reviews (
    review_id           NUMBER(6) GENERATED ALWAYS AS IDENTITY CONSTRAINT review_pk PRIMARY KEY,
    product_review      NUMBER(1),
    review_flag         NUMBER(1),
    review_description  VARCHAR2(250),
    customer_id         NUMBER(6),
    product_id          NUMBER(6),
    
    CONSTRAINT FK_customer2
        FOREIGN KEY(customer_id)
        REFERENCES SS_Customers(customer_id),
        
    CONSTRAINT FK_product2
        FOREIGN KEY(product_id)
        REFERENCES SS_Products(product_id)  
);
/*creating warehouses table*/
CREATE TABLE SS_Warehouses ( 
    warehouse_id    NUMBER(6) GENERATED ALWAYS AS IDENTITY CONSTRAINT warehouse_pk PRIMARY KEY,
    name            VARCHAR2(30),
    address         VARCHAR2(70)
);
/*creating table which holds which products are in which table*/
CREATE TABLE Warehouse_Products (
    warehouse_id    NUMBER(6),
    product_id      NUMBER(6),
    product_quantity    NUMBER(7),
    
    CONSTRAINT FK_warehouse
        FOREIGN KEY(warehouse_id)
        REFERENCES SS_Warehouses(warehouse_id),
        
    CONSTRAINT FK_product3
        FOREIGN KEY(product_id)
        REFERENCES SS_Products(product_id)   
);
/*creating table to audit orders*/
CREATE TABLE SS_AuditOrders (
    audit_id   NUMBER(6) GENERATED ALWAYS AS IDENTITY CONSTRAINT audit_pk PRIMARY KEY,
    order_id   NUMBER(6) ,
    date_ordered    DATE,
    
    CONSTRAINT FK_order
        FOREIGN KEY(order_id)
        REFERENCES SS_Orders(order_id)
);
/
--CUSTOMER TABLE INSERT VALUES
INSERT INTO SS_Customers (firstname, lastname, email, address)
VALUES('Alex','Brown','alex@gmail.com','boul saint laurent, montreal, quebec, canada');

INSERT INTO SS_Customers (firstname, lastname, email, address)
VALUES('Amanda','Harry','am.harry@yahoo.com','100 boul saint laurent, montreal, quebec, canada');

INSERT INTO SS_Customers (firstname, lastname, email, address)
VALUES('Ari','Brown','b.a@gmail.com','');

INSERT INTO SS_Customers (firstname, lastname, email, address)
VALUES('Daneil','Hanne','daneil@yahoo.com','100 atwater street, toronto, canada');

INSERT INTO SS_Customers (firstname, lastname, email, address)
VALUES('Jack','Johnson','johnson.a@gmail.com','Calgary, Alberta, Canada');

INSERT INTO SS_Customers (firstname, lastname, email, address)
VALUES('John','Boura','bdoura@gmail.com','100 Young street, toronto, canada');

INSERT INTO SS_Customers (firstname, lastname, email, address)
VALUES('John','Belle','abcd@yahoo.com','105 Young street, toronto, canada');

INSERT INTO SS_Customers (firstname, lastname, email, address)
VALUES('Mahsa','Sadeghi','msadeghi@dawsoncollege.qc.ca','dawson college, montreal, qeuebe, canada');

INSERT INTO SS_Customers (firstname, lastname, email, address)
VALUES('Martin','Li','m.li@gmail.com','87 boul saint laurent, montreal, quebec, canada');

INSERT INTO SS_Customers (firstname, lastname, email, address)
VALUES('Martin','Alexander','marting@yahoo.com','brossard, quebec, canada');

INSERT INTO SS_Customers (firstname, lastname, email, address)
VALUES('Noah','Garcia','g.noah@yahoo.com','22222 happy street, Laval, quebec, canada');

INSERT INTO SS_Customers (firstname, lastname, email, address)
VALUES('Olivia','Smith','smith@hotmail.com','76 boul decalthon, laval, quebec, canada');

--PRODUCTS TABLE INSERT VALUES
INSERT INTO SS_Products (name, category)
VALUES('Apple','Grocery');

INSERT INTO SS_Products (name, category)
VALUES('Barbie Movie','DVD');

INSERT INTO SS_Products (name, category)
VALUES('BMWi6','Cars');

INSERT INTO SS_Products (name, category)
VALUES('BMWiX','Toys');

INSERT INTO SS_Products (name, category)
VALUES('Chicken','Grocery');

INSERT INTO SS_Products (name, category)
VALUES('Lamborghini Lego','Toys');

INSERT INTO SS_Products (name, category)
VALUES('Laptop ASUS 104S','Electronics');

INSERT INTO SS_Products (name, category)
VALUES('LOreal Normal Hair','Health');

INSERT INTO SS_Products (name, category)
VALUES('Orange','Grocery');

INSERT INTO SS_Products (name, category)
VALUES('Paper Towel','Beauty');

INSERT INTO SS_Products (name, category)
VALUES('Pasta','Grocery');

INSERT INTO SS_Products (name, category)
VALUES('Plum','Grocery');

INSERT INTO SS_Products (name, category)
VALUES('PS5','Electronics');

INSERT INTO SS_Products (name, category)
VALUES('SIMS CD','Video Games');

INSERT INTO SS_Products (name, category)
VALUES('Truck 500c','Vehicle');

INSERT INTO SS_Products (name, category)
VALUES('Tomato','Grocery');

INSERT INTO SS_Products (name, category)
VALUES('Train X745','Train');

--ORDERS TABLE INSERT VALUE
INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2023/4/21',1,970,'marche adonis',8,7);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2023/10/23',2,10,'marche atwater',1,1);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2023/10/1',3,50,'dawson store',10,14);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2023/10/23',1,2,'store magic',4,9);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2023/10/23',1,30,'movie store',1,2);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2023/10/10',1,10,'super rue champlain',10,8);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2023/10/11',1,40,'toy r us',8,4);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2023/10/10',1,50000,'Dealer one',6,3);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('',1,856600,'dealer montreal',3,15);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('',3,50,'movie start',2,10);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2020/5/6',6,10,'marche atwater',5,12);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2019/9/12',3,30,'super rue champlain',10,8);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2010/10/11',1,40,'toy r us',8,6);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2022/5/6',7,10,'marche atwater',8,12);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2023/10/7',2,80,'toy r us',8,6);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2023/8/10',1,50000,'Dealer one',7,3);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2023/10/23',1,16,'movie store',1,14);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2023/10/2',1,45,'toy r us',1,2);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2019/4/3',1,9.5,'marche adonis',9,5);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2021/12/29',3,13.5,'marche atwater',12,11);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2020/1/20',1,200,'star store',11,13);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2022/10/11',1,38,'toy r us',8,4);

INSERT INTO SS_Orders (date_ordered,order_quantity,order_price ,store_ordered,customer_id,product_id)
VALUES('2021/12/29',3,15,'store magic',12,11);

--REVIEWS TABLE INSERT VALUES
INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(5,'','',12,11);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(4,'','',12,12);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(3,0,'quality was not good',1,1);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(1,0,'',1,2);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(5,0,'highly recommend',4,9);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(1,0,'',1,14);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(1,0,'missing some parts',8,4);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(5,1,'trash',6,3);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(1,0,'did not worth the price',10,8);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(1,0,'great product',8,6);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(4,0,'',1,2);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(2,1,'',10,14);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(5,1,'bad quality',7,3);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(4,0,'it was affordable',8,7);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(1,2,'worse car i have ever droven!',8,4);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(4,'','',8,12);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(4,'','',5,12);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(3,'','',10,8);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(4,'','',9,5);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(1,0,'missing some parts',8,6);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(5,'','',2,10);

INSERT INTO SS_Reviews(product_review,review_flag,review_description,customer_id ,product_id)
VALUES(2,'','',3,15);

--WAREHOUSES TABLE INSERT VALUES

INSERT INTO SS_Warehouses(name,address)
VALUES('Warehouse A','100 rue William, saint laurent, Quebec, Canada');

INSERT INTO SS_Warehouses(name,address)
VALUES('Warehouse B','304 Rue Fran�ois-Perrault, Villera Saint-Michel, Montr�al, QC');

INSERT INTO SS_Warehouses(name,address)
VALUES('Warehouse C','86700 Weston Rd, Toronto, Canada');

INSERT INTO SS_Warehouses(name,address)
VALUES('Warehouse D','170  Sideroad, Quebec City, Canada');

INSERT INTO SS_Warehouses(name,address)
VALUES('Warehouse E','1231 Trudea road, Ottawa, Canada ');

INSERT INTO SS_Warehouses(name,address)
VALUES('Warehouse F','16  Whitlock Rd, Alberta, Canada');

-- INSERT INTO Warehouse_Products VALUES
INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(1,7,1000);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(2,1,24980);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(3,14,103);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(4,9,35405);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(5,2,40);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(6,8,450);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(1,4,10);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(1,3,6);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(5,15,1000);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(6,10,3532);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(3,12,43242);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(2,10,39484);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(4,12,6579);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(5,6,98765);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(6,5,43523);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(1,11,2132);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(4,13,123);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(1,16,352222);

INSERT INTO Warehouse_Products(warehouse_id,product_id, product_quantity) 
VALUES(5,17,4543);

/
-- CREATE TYPE Area

CREATE TYPE CUSTOMERS_TYPE AS OBJECT(
    firstname   VARCHAR2(30),
    lastname    VARCHAR2(30),
    email       VARCHAR2(40),
    address     VARCHAR2(50)
);
/

CREATE TYPE PRODUCTS_TYPE AS OBJECT (
    name        VARCHAR2(30),
    category    VARCHAR2(40)
);
/

CREATE TYPE ORDERS_TYPE AS OBJECT (
    date_ordered    DATE,
    order_quantity  NUMBER(7,0),
    order_price     NUMBER(7,0),
    store_ordered   VARCHAR2(40),
    customer_id     NUMBER(6,0),
    product_id      NUMBER(6,0)
);
/

CREATE TYPE REVIEWS_TYPE AS OBJECT (
    product_review      NUMBER(6,0),
    review_flag         NUMBER(1,0),
    review_description  VARCHAR2(250),
    customer_id         NUMBER(6,0),
    product_id          NUMBER(6,0)
);
/

CREATE TYPE WAREHOUSES_TYPE AS OBJECT (
    name    VARCHAR2(30),
    address VARCHAR2(70)
);
/

CREATE TYPE WAREHOUSE_PRODUCTS_TYPE AS OBJECT (
    warehouse_id        NUMBER(6,0),
    product_id          NUMBER(6,0),
    product_quantity    NUMBER(7,0)
);
/

-- CREATE TRIGGER Area

CREATE OR REPLACE TRIGGER AuditOrderTrigger AFTER INSERT
ON SS_Orders 
FOR EACH ROW
BEGIN
        INSERT INTO SS_AuditOrders(order_id,date_ordered)
            VALUES(:new.order_id,SYSDATE);
END;
/

CREATE OR REPLACE TRIGGER MonitorReviews AFTER UPDATE
ON SS_Reviews
BEGIN
        DELETE FROM SS_Reviews
        WHERE review_flag > 2;
END;
/