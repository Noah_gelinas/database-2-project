package databaseproject;

import java.util.Map;
import java.sql.SQLException;
import java.sql.SQLData;
import java.sql.SQLOutput;
import java.sql.SQLInput;
import java.sql.Connection;
import java.sql.CallableStatement;

/**
 * A WarehouseProducts class that represents a WarehouseProducts table that includes its constructor, 
 * its fields' getters and setters, and overrides of SQLData to allow object serialization. 
 */
public class WarehouseProducts implements SQLData {
  private int warehouse_id;
  private int product_id;
  private int product_quantity;

  public WarehouseProducts(int warehouse_id,int product_id, int product_quantity) {
    this.warehouse_id=warehouse_id;
    this.product_id=product_id;
    this.product_quantity=product_quantity;
  }

  public int getWarehouse_id () {
    return this.warehouse_id;
  }

  public int getProduct_id () {
    return this.product_id;
  }

  public int getProduct_Quantity() {
    return this.product_quantity;
  }

  public void setWareHouseID(int warehouse_id) {
    this.warehouse_id = warehouse_id;
  }

  public void setProductID(int product_id) {
    this.product_id = product_id;
  }

  public void setProductQuantity(int product_quantity) {
    this.product_quantity = product_quantity;
  }

  @Override
  public String getSQLTypeName() throws SQLException {
    return "WAREHOUSE_PRODUCTS_TYPE";
  }

  @Override
  public void writeSQL(SQLOutput stream) throws SQLException {
    stream.writeInt(getWarehouse_id());
    stream.writeInt(getProduct_id());
    stream.writeInt(getProduct_Quantity());
  }

  @Override
  public void readSQL(SQLInput stream, String typeName) throws SQLException {
    setWareHouseID(stream.readInt());
    setProductID(stream.readInt());
    setProductQuantity(stream.readInt());
  }

  public void addToDataBase(Connection conn) throws SQLException, ClassNotFoundException {
    Map<String, Class<?>> map = conn.getTypeMap();
    conn.setTypeMap(map);
    map.put("WAREHOUSE_PRODUCTS_TYPE", Class.forName("databaseproject.WarehouseProducts"));
    WarehouseProducts warehouseProducts = new WarehouseProducts(this.warehouse_id, this.product_id, this.product_quantity);
    String  sql = "{call insert_package.add_warehouse_products(?)}";
    try(CallableStatement stmt =
      conn.prepareCall(sql)
    ) {
        stmt.setObject(1, warehouseProducts);
        stmt.execute();
      }
  }

  @Override
  public String toString() {
    return ("Warehouse ID: " + this.warehouse_id + "\nProduct ID: " + this.product_id + "\nProduct Quantity: " + this.product_quantity + "\n");
  }
}
