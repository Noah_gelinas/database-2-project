package databaseproject;

import java.sql.*;
import java.util.*;

public class App {
  private final static Scanner SCAN = new Scanner(System.in); 

  /**
   * The main method where displays are, used to ask user to log in, determine access, 
   * and ask commands and relative information to use.
   * 
   * @param args
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public static void main(String[] args) throws SQLException, ClassNotFoundException {
    int userControl = 0;
    boolean isNumeric = false;
    char action = 'R';
    boolean isEmployee = false;
    boolean exit = false;
    String username="";
    String password="";

    boolean isEnterKey = true;
    while (isEnterKey) {
      System.out.println("\nEnter Username: ");
      username = SCAN.nextLine();
      if (username.equals("")){
        isEnterKey=true;
        System.out.println("Incorrect Input!");
      }
      else {
        isEnterKey=false;
      }
    }

    isEnterKey=true;
    while (isEnterKey) {
      password = new String(System.console().readPassword("\nPassword: "));

       if (password.equals("")){
        isEnterKey=true;
        System.out.println("\nIncorrect Input!");
      }
      else {
        isEnterKey=false;
      }
    }

    SuperStoreServices service = new SuperStoreServices();
    service.getConnection(username,password);

    while (!isNumeric) {
      try {
        System.out.println("\nPress '0' for Customer or '1' for Employee");
        userControl = SCAN.nextInt();
        SCAN.nextLine();
        isNumeric = true;
      }
      catch (InputMismatchException ime) {
        System.out.println("\nInvalid character found, Please enter numeric values only !!");
        isNumeric = false;
        userControl = 1;
        SCAN.nextLine();
      }

      if (userControl > 1 || userControl < 0) {
        System.out.println("\nOnly enter '0' for Employee or '1' for Manager!");
        isNumeric = false;
      }
    }

    if (userControl == 1) {
      isEmployee = true;
    }

    while(!exit) {
      boolean isChar = false;
      isNumeric = false;
      String input="";
      isEnterKey=true;
      while(isEnterKey) {
        System.out.println("What do you want to do? 'S' = Read By Id | 'R' = Read | 'I' = Insert | 'D' = Delete | 'U' = Update | 'E' = Exit" );
        input = SCAN.nextLine();

        if (input.equals("")) {
          System.out.println("Incorrect Input!");
          isEnterKey=true;
        }
        else{
          isEnterKey=false;
        }
      }
      while (!isChar) {
        action = input.charAt(0);
        if(input.length() != 1 || action != 'R' && action != 'I' && action != 'D' && action != 'U' && action != 'E' && action!='S') {
          System.out.println("Input too long or not valid! Please Enter 'S' = Read By Id | 'R' = Read | 'I' = Insert | 'D' = Delete | 'U' = Update | 'E' = Exit");
          input = SCAN.next();
        } else {
          isChar = true;
        }
      }

      if (action == 'R') {
        read(service);
      }
      else if(action=='S') {
        specificRead(service);
      }
      else if (action == 'I') {
        insert(service, isEmployee);
      }
      else if(action == 'D') {
        delete(service, isEmployee);
      }
      else if (action == 'U') {      
        update(service, isEmployee);
      }
      else if (action == 'E') {
        System.out.println("\nExiting...\n");
        exit = true;
      }
    }  
    service.close();
  }
  
  /**
   * This reads a specified table's row using their ID.
   * 
   * @param service a SuperStoreService object
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public static void specificRead(SuperStoreServices service) throws SQLException, ClassNotFoundException {
    int table=0;
    boolean isNumeric=false;  
    while (!isNumeric) {
          try {
            System.out.println("Read What? : '0' = Customers | '1' = Products | '2' = Reviews | '3' = Orders | '4' = Warehouses | '5' = Warehouse Products By Product id | '6' = Audit Orders | '7' = Warehouse Products By Warehouse Id");
            table = SCAN.nextInt();
            isNumeric = true;
          }
          catch (InputMismatchException ime) {
            System.out.println("Invalid character found, Please enter numeric values only!");
            isNumeric = false;
            table = 0;
            SCAN.nextLine();
          }

          if (table > 7 || table < 0) {
            System.out.println("Invalid number!");
            isNumeric = false;
          }
        }
        if (table == 0) {
          System.out.println("\nEnter customer id to find...\n");
          int id=SCAN.nextInt();
          service.readCustomerById(id);
        }
        else if (table == 1) {
          System.out.println("\nEnter product id to find...\n");
          int id=SCAN.nextInt();
          service.readProductsById(id);
        }
        else if (table == 2) {
          System.out.println("\nEnter review id to find...\n");
          int id=SCAN.nextInt();
          service.readReviewsById(id);     
        }
        else if (table == 3) {
          System.out.println("\nEnter order id to find...\n");
          int id=SCAN.nextInt();
          service.readOrdersById(id);
        }
        else if (table == 4) {
          System.out.println("\nEnter warehouse id to find...\n");
          int id=SCAN.nextInt();
          service.readWarehousesById(id);
        }
        else if (table == 5) {
          System.out.println("\nEnter warehouse_product product id to find\n");
          int id=SCAN.nextInt();
          service.readWarehouseProductsByProdId(id);
        }
        else if (table == 6) {
          System.out.println("\nEnter audit id to find\n");
          int id=SCAN.nextInt();
          service.readAuditById(id);
        }
        else if (table == 7) {
          System.out.println("\nEnter warehouse_product warehouse id to find\n");
          int id=SCAN.nextInt();
          service.readWarehouseProductsByWareId(id);
        }
  }

  /**
   * This is used to read all rows of a specified table.
   * 
   * @param service a SuperStoreService object
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public static void read(SuperStoreServices service) throws SQLException, ClassNotFoundException {
    int table = 0;
    boolean isNumeric = false;
    while (!isNumeric) {
      try {
        System.out.println("Read What? : '0' = Customers | '1' = Products | '2' = Reviews | '3' = Orders | '4' = Warehouses | '5' = Warehouse Products | '6' = Audit Orders");
        table = SCAN.nextInt();
        SCAN.nextLine();
        isNumeric = true;
      }
      catch (InputMismatchException ime) {
        System.out.println("Invalid character found, Please enter numeric values only!");
        isNumeric = false;
        table = 0;
        SCAN.nextLine();
      }

      if (table > 6 || table < 0) {
        System.out.println("Invalid number!");
        isNumeric = false;
      }
    }

    if (table == 0) {
      System.out.println("\nReading customers...\n");
      service.readCustomers();
    }
    else if (table == 1) {
      System.out.println("\nReading products...\n");
      service.readProducts();
    }
    else if (table == 2) {
      System.out.println("\nReading reviews...\n");   
      service.readReviews();     
    }
    else if (table == 3) {
      System.out.println("\nReading orders...\n");
      service.readOrders();
    }
    else if (table == 4) {
      System.out.println("\nReading warehouses...\n");
      service.readWarehouses();
    }
    else if (table == 5) {
      System.out.println("\nReading warehouse products...\n");
      service.readWarehouseProducts();
    }
    else if (table == 6) {
      System.out.println("\nReading order history...\n");
      service.readAuditOrders();
    }
  }

  /**
   * This inserts a new row to a specified table.
   * 
   * @param service a SuperStoreService object
   * @param isEmployee a boolean to know if a user is an employee or not
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public static void insert(SuperStoreServices service, boolean isEmployee) throws SQLException, ClassNotFoundException {
    int table = 0;
    int tableCustomers=0;
    boolean isNumeric = false;

    while (!isNumeric) {
      try {
        if(isEmployee){
          System.out.println("Insert What? : '0' = Customers | '1' = Products | '2' = Reviews | '3' = Orders | '4' = Warehouse | '5' = Products to a warehouse");
          table = SCAN.nextInt();
          SCAN.nextLine();
          isNumeric = true;
        }else{
          System.out.println("Insert What? : '0' = Customers | '1' = Reviews | '2' = Orders");
          tableCustomers = SCAN.nextInt();
          SCAN.nextLine();
          isNumeric = true;
        }
      }
      catch (InputMismatchException ime) {
        System.out.println("Invalid character found, Please enter numeric values only !!");
        isNumeric = false;
        table = 0;
        tableCustomers=0;
        SCAN.nextLine();
      }

      if (table > 5 || table < 0 || tableCustomers>2||tableCustomers<0) {
        System.out.println("Invalid number!");
        isNumeric = false;
      }
    }

    if (table == 0 && tableCustomers==0) {
      System.out.println("\nInserting a new customer...\n");
      service.addCustomer();
    }
    else if (table == 2 || tableCustomers == 1) {
      System.out.println("\nInserting a new review...\n");
      service.addReview();        
    }
    else if (table == 3 || tableCustomers == 2) {
      System.out.println("\nInserting a new order...\n");
      service.addOrder();
    }
    else if (isEmployee) {
      if (table == 1) {
        System.out.println("\nInserting a new product...\n"); 
        service.addProduct();
      }else if(table == 4) {
        System.out.println("\nInserting new Warehouse...\n");
        service.addWarehouse();
      }
      else if(table == 5) {
        System.out.println("\nInserting a product to a warehouse...\n"); 
        service.addWarehouseProducts();
      }
    }
  }

  /**
   * This deletes a row of a specified table using their ID.
   * 
   * @param service a SuperStoreService object
   * @param isEmployee a boolean to know if a user is an employee or not
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public static void delete(SuperStoreServices service, boolean isEmployee) throws SQLException, ClassNotFoundException {
    int table = 0;
    int tableCustomers=0;
    boolean isNumeric = false;

    while (!isNumeric) {
      try {
        if(isEmployee) {
          System.out.println("Delete From What? : '0' = Customers | '1' = Products | '2' = Reviews | '3' = Orders | '4' = Warehouses");
          table = SCAN.nextInt();
          tableCustomers=1;
          isNumeric = true;
        }else{
          System.out.println("Sorry! You do not have access to this command.");
          tableCustomers = 1;
          isNumeric = true;
        }
      }
      catch (InputMismatchException ime) {
        System.out.println("Invalid character found, Please enter numeric values only !!");
        isNumeric = false;
        table = 0;
        tableCustomers=0;
        SCAN.nextLine();
      }

      if (table > 4 || table < 0 || tableCustomers !=1) {
        System.out.println("Invalid number!");
        isNumeric = false;
      }
    }

    if (isEmployee) {
      if (table==0) {
        System.out.println("\nEnter Customer ID to delete: ");
        SCAN.nextLine();
        int cus_id=SCAN.nextInt();
        System.out.println("\nDeleting Customer, their Reviews, and Orders From Database");
        service.deleteCustomer(cus_id);
      }else if(table==1) {
        System.out.println("\nEnter Product ID to delete: ");
        SCAN.nextLine();
        int product_id=SCAN.nextInt();
        System.out.println("\nDeleting Product From Database");
        service.deleteProduct(product_id);
      }else if(table==3) {
        System.out.println("\nEnter order ID to delete: ");
        SCAN.nextLine();
        int order_id=SCAN.nextInt();
        System.out.println("\nDeleting Order From Database");
        service.deleteOrder(order_id);
      }else if(table==4) {
        System.out.println("\nEnter Warehouse ID to delete: ");
        SCAN.nextLine();
        int warehouse_id=SCAN.nextInt();
        System.out.println("\nDeleting Warehouse From Database");
        service.deleteWarehouse(warehouse_id);
      }
    }
  }

  /**
   * This updates a specified table's row using its ID.
   * @param service a SuperStoreService object
   * @param isEmployee a boolean to know if a user is an employee or not
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public static void update(SuperStoreServices service, boolean isEmployee) throws SQLException, ClassNotFoundException {
    int table = 0;
    int tableCustomers=0;
    boolean isNumeric = false;
    
    while (!isNumeric) {
      try {
        if (isEmployee) {
          System.out.println("\nUpdate What? : '0' = Reviews (review flag) | '1' = Warehouses (address)| '2' = warehouse_products (quantity)");
          table = SCAN.nextInt();
          tableCustomers = 1;
          isNumeric = true;
        }else{
          System.out.println("\nUpdate What? : '1' = Customers (name and address)");
          tableCustomers = SCAN.nextInt();
          isNumeric = true;
        }
      }
      catch (InputMismatchException ime) {
        System.out.println("\nInvalid character found, Please enter numeric values only !!");
        isNumeric = false;
        table = 0;
        tableCustomers=0;
        SCAN.nextLine();
      }
      if (table > 3 || table < 0 || tableCustomers != 1) {
        System.out.println("\nInvalid number!");
        isNumeric = false;
      }
    }
    if (!isEmployee) {
      if (tableCustomers == 1) {
        System.out.println("\nEnter Customer id to update");
        SCAN.nextLine();
        int cus_id=SCAN.nextInt();
        System.out.println("\nEnter new email");
        SCAN.nextLine();
        String email=SCAN.nextLine();
        System.out.println("\nEnter new address");
        String address=SCAN.nextLine();
        System.out.println("\nUpdating values");
        service.updateCustomers(cus_id,email,address);
      }
    }else {
      if (table == 2) {
        System.out.println("\nEnter Warehouse id to update");
        SCAN.nextLine();
        int wh_id= SCAN.nextInt();
        System.out.println("\nEnter Product id to update ");
        SCAN.nextLine();
        int prod_id=SCAN.nextInt();
        System.out.println("\nEnter new product quantity");
        SCAN.nextLine();
        int newQuant=SCAN.nextInt();
        System.out.println("\nUpdating values");
        service.updateWarehouseProducts(wh_id,prod_id,newQuant);
      }
      else if(table == 0) {
        System.out.println("\nEnter Review ID to flag");
        SCAN.nextLine();
        int revId=SCAN.nextInt();
        System.out.println("\nUpdating Values");
        service.updateReview(revId);
      }
      else if(table == 1) {
        System.out.println("\nEnter warehouse id to update");
        SCAN.nextLine();
        int wh_id=SCAN.nextInt();
        System.out.println("Enter new warehouse address");
        SCAN.nextLine();
        String address=SCAN.nextLine();
        System.out.println("\nUpdating Values");
        service.updateWarehouse(wh_id, address);
      }
    }
  }
}
