package databaseproject;

import java.util.Map;
import java.sql.SQLException;
import java.sql.SQLData;
import java.sql.SQLOutput;
import java.sql.SQLInput;
import java.sql.Connection;
import java.sql.CallableStatement;

/**
 * A Warehouse class that represents a Warehouse table that includes its constructor, 
 * its fields' getters and setters, and overrides of SQLData to allow object serialization. 
 */
public class Warehouse implements SQLData {
  private String name;
  private String address;

  public Warehouse(String name, String address) {
    this.name=name;
    this.address=address;
  }

  public Warehouse(int warehouse_id, String name, String address) {
    this(name, address);
    this.address=address;
  }

  public String getName() {
    return this.name;
  }

  public String getAddress() {
    return this.address;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  @Override
  public String getSQLTypeName() throws SQLException {
    return "WAREHOUSES_TYPE";
  }

  @Override
  public void writeSQL(SQLOutput stream) throws SQLException {
    stream.writeString(getName());
    stream.writeString(getAddress());
  }

  @Override
  public void readSQL(SQLInput stream, String typeName) throws SQLException {
    setName(stream.readString());
    setAddress(stream.readString());
  }

  public void addToDataBase(Connection conn) throws SQLException, ClassNotFoundException {
    Map<String, Class<?>> map = conn.getTypeMap();
    conn.setTypeMap(map);
    map.put("WAREHOUSES_TYPE", Class.forName("databaseproject.Orders"));
    Warehouse warehouse = new Warehouse(this.name, this.address);
    String sql = "{call insert_package.add_warehouse(?)}";
    try(CallableStatement stmt =
    conn.prepareCall(sql)
    ) {
        stmt.setObject(1, warehouse);
        stmt.execute();
      }
  }

  @Override
  public String toString() {
    return("\nName: " + this.name + "\nAddress: " + this.address + "\n");
  }
}
