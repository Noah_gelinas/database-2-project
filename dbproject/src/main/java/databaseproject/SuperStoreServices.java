package databaseproject;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.CallableStatement;
import java.sql.Types;
import java.time.LocalDate;
import java.time.ZoneId;
import java.sql.Date;
import oracle.jdbc.OracleTypes;
import java.sql.SQLIntegrityConstraintViolationException;

import java.util.InputMismatchException;
import java.util.Scanner;

public class SuperStoreServices {
  private Connection conn;
  private final Scanner SCAN;
  public SuperStoreServices() {
    this.SCAN = new Scanner(System.in);
  } 
  
  /**
   * This establishes the connection to JDBC.
   * 
   * @param uName the user's username
   * @param password the user's password
   */
  public void getConnection(String uName, String password) {
    try{
      if(!this.alreadyConnected()){
        conn = DriverManager.getConnection("jdbc:oracle:thin:@198.168.52.211:1521/pdbora19c.dawsoncollege.qc.ca", uName, password);
      }
    } catch (SQLException e) {
      e.printStackTrace();
      System.out.println("Error in logging in! Please run the program again.");
      System.exit(0);
    }
  }

  /**
   * This checks if user is already connected to the JDBC server.
   * 
   * @return a boolean that determines if a user is already connected
   * @throws SQLException
   */
  public boolean alreadyConnected() throws SQLException {
    boolean alreadyConnected = false;
    if (conn != null) {
      alreadyConnected=true;
    }
    return alreadyConnected;
  }  

  public void close() throws SQLException {
    System.out.println("closing connection...");
    conn.close();
    System.out.println("connection closed");
  }

  // READ METHODS
  
  /**
   * This reads the Customers table.
   * 
   * @throws SQLException
   */
  public void readCustomers() throws SQLException {
    ResultSet rs = null;
    String sql= "{? = call read_package.read_customers}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.execute();
      rs=(ResultSet) stmt.getObject(1);
      while (rs.next()) {
        int cus_id=rs.getInt("customer_id");
        int numOrd=0;
        String numOrders="{? = call read_package.countOrders(?)}";
        try(CallableStatement stmt2=conn.prepareCall(numOrders)) {
          stmt2.registerOutParameter(1, Types.INTEGER);
          stmt2.setInt(2,cus_id);
          stmt2.execute();
          numOrd=stmt2.getInt(1);
        }catch (SQLException e) {
          e.printStackTrace();
        }
        System.out.println("\nCustomer ID: " + rs.getInt("customer_id")+ "\nFirst Name: "+ rs.getString("firstname") + "\nLast name: "+ rs.getString("lastname")+ "\nEmail: " + rs.getString("email") + "\nAddress: "+rs.getString("address")+"\nExisting orders: "+ numOrd +"\nFor more details on orders, Read from orders and input customer id!\n");
      }
    }catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * This reads the Products table.
   * 
   * @throws SQLException
   */
  public void readProducts() throws SQLException {
    ResultSet rs = null;
    String sql= "{? = call read_package.read_products}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.execute();
      rs=(ResultSet) stmt.getObject(1);
      while (rs.next()) {
        int prod_id=rs.getInt("product_id");
        String rev="{? = call read_package.avgReview(?)}";
        String totStock="{?=call read_package.totalInv(?)}";
        String totOrd="{?=call read_package.totalOrdered(?)}";
        double avgReview=0;
        int totalStock=0;
        int totalOrd=0;
        try(CallableStatement avgRev=conn.prepareCall(rev)) {
          avgRev.registerOutParameter(1, Types.DOUBLE);
          avgRev.setInt(2,prod_id);
          avgRev.execute();
          avgReview=avgRev.getDouble(1);
        }catch(SQLException e) {
          e.printStackTrace();
        }
        try(CallableStatement totalStockProd=conn.prepareCall(totStock)) {
          totalStockProd.registerOutParameter(1, Types.INTEGER);
          totalStockProd.setInt(2,prod_id);
          totalStockProd.execute();
          totalStock=totalStockProd.getInt(1);
        }catch( SQLException e) {
          e.printStackTrace();
        }
        try(CallableStatement totalOrders=conn.prepareCall(totOrd)) {
          totalOrders.registerOutParameter(1, Types.INTEGER);
          totalOrders.setInt(2,prod_id);
          totalOrders.execute();
          totalOrd=totalOrders.getInt(1);
        }catch(SQLException e) {
          e.printStackTrace();
        }
        System.out.println("\nProduct ID: " + rs.getInt("product_id") + "\nName: " + rs.getString("name")+"\nCategory: " + rs.getString("category")+ "\nAverage Review Score: "+avgReview+ "\nTotal Stock Left: "+ totalStock+"\nTimes Ordered: "+totalOrd);
      }
    }catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * This reads the Reviews table.
   * 
   * @throws SQLException
   */
  public void readReviews() throws SQLException {
    ResultSet rs = null;
    String sql= "{? = call read_package.read_reviews}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.execute();
      rs=(ResultSet) stmt.getObject(1);
      while(rs.next()) {
        System.out.println("\nReview ID: " + rs.getInt("review_id")+"\nProduct Review: "+rs.getInt("product_review")+"\nReview Flag Number: "+rs.getInt("review_flag")+"\nReview Description: "+rs.getString("review_description")+"\nCustomer ID corresponding with this review: "+rs.getInt("customer_id")+"\nProduct ID corresponding with this review: "+rs.getInt("product_id"));
      }
    }catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * This reads the Orders table.
   * 
   * @throws SQLException
   */
  public void readOrders() throws SQLException {
    ResultSet rs = null;
    String sql= "{? = call read_package.read_orders}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.execute();
      rs=(ResultSet) stmt.getObject(1);
      while(rs.next()) {
        System.out.println("\nOrder ID: " + rs.getInt("order_id")+"\nDate of Order: "+rs.getDate("date_ordered")+"\nOrder Quantity: "+rs.getInt("order_quantity")+"\nPrice of Order: $"+rs.getInt("order_price")+"\nStore Ordered From: "+rs.getString("store_ordered")+"\nCustomer ID corresponding with this order: "+rs.getInt("customer_id")+"\nThe ID of product ordered: "+rs.getInt("product_id"));
      }
    }catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * This reads the Warehouses table.
   * 
   * @throws SQLException
   */
  public void readWarehouses() throws SQLException {
    ResultSet rs = null;
    String sql= "{? = call read_package.read_warehouses}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.execute();
      rs=(ResultSet) stmt.getObject(1);
      while(rs.next()) {
        System.out.println("\nWarehouse ID: "+rs.getInt("warehouse_id")+"\nWarehouse name: "+rs.getString("name")+"\nAddress: "+rs.getString("address"));
      }
    }catch(SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * This reads the WarehouseProducts table.
   * 
   * @throws SQLException
   */
  public void readWarehouseProducts() throws SQLException {
    ResultSet rs = null;
    String sql= "{? = call read_package.read_warehouseProducts}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.execute();
      rs=(ResultSet) stmt.getObject(1);
      while(rs.next()) {
        System.out.println("\nWarehouse ID: "+rs.getInt("warehouse_id")+"\nProduct ID: "+rs.getInt("product_id")+"\nProduct Quantity: "+rs.getInt("product_quantity"));
      }
    }catch(SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * This reads the AuditOrders table.
   * 
   * @throws SQLException
   */
  public void readAuditOrders() throws SQLException {
    ResultSet rs = null;
    String sql= "{? = call read_package.read_auditOrders}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.execute();
      rs=(ResultSet) stmt.getObject(1);
      while(rs.next()) {
        System.out.println("\nAudit ID: "+rs.getInt("audit_id")+"\nOrder ID: "+rs.getInt("order_id")+"\nDate Of New Order Creation: "+rs.getDate("date_ordered") + "\n");
      }
    }catch (SQLException e){
      e.printStackTrace();
    }
  }

  /**
   * This reads a row of the Customers table by ID.
   *  
   * @param cus_id the customer ID of the row to read
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public void readCustomerById(int cus_id) throws SQLException, ClassNotFoundException {
    ResultSet rs=null;
    String sql="{?=call read_package.read_customersById(?)}";
    try (CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.setInt(2, cus_id);
      stmt.execute();
      rs=(ResultSet) stmt.getObject(1);
      while(rs.next()) {
        System.out.println("\nCustomer ID:" + rs.getInt("customer_id")+ "\nFirst Name: "+ rs.getString("firstname") + "\nLast name: "+ rs.getString("lastname")+ "\nEmail: " + rs.getString("email") + "\nAddress: "+rs.getString("address"));
      }
    }catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * This reads a row of the AuditOrders table by ID.
   * 
   * @param aud_id the audit ID of the row to read
   * @throws SQLException
   */
  public void readAuditById(int aud_id) throws SQLException {
    ResultSet rs = null;
    String sql= "{? = call read_package.read_auditById(?)}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.setInt(2,aud_id);
      stmt.execute();
      rs=(ResultSet) stmt.getObject(1);
      while(rs.next()) {
        System.out.println("\nAudit ID: "+rs.getInt("audit_id")+"\nOrder ID: "+rs.getInt("order_id")+"\nDate Of New Order Creation: "+rs.getDate("date_ordered"));
      }
    }catch (SQLException e){
      e.printStackTrace();
    }
  }  

  /**
   * This reads a row of the Orders table by ID.
   * 
   * @param order_id the order ID of the row to read
   * @throws SQLException
   */
  public void readOrdersById(int order_id) throws SQLException {
    ResultSet rs = null;
    String sql= "{? = call read_package.read_ordersById(?)}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.setInt(2,order_id);
      stmt.execute();
      rs=(ResultSet) stmt.getObject(1);
      while(rs.next()) {
        System.out.println("\nOrder ID: " + rs.getInt("order_id")+"\nDate of Order: "+rs.getDate("date_ordered")+"\nOrder Quantity: "+rs.getInt("order_quantity")+"\nPrice of Order: "+rs.getInt("order_price")+"\nStore Ordered From: "+rs.getString("store_ordered")+"\nCustomer ID corresponding with this order: "+rs.getInt("customer_id")+"\nThe ID of product ordered: "+rs.getInt("product_id"));
      }
    }catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * This reads a row of the Products table by ID.
   * 
   * @param prod_id the product ID of the row to read
   * @throws SQLException
   */
  public void readProductsById(int prod_id) throws SQLException {
    ResultSet rs = null;
    String sql= "{? = call read_package.read_productsById(?)}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.setInt(2,prod_id);
      stmt.execute();
      rs=(ResultSet) stmt.getObject(1);
      while (rs.next()) {
        System.out.println("\nProduct ID: " + rs.getInt("product_id") + "\nName: " + rs.getString("name")+"\nCategory: " + rs.getString("category"));
      }
    }catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * This reads a row of the Reviews table by ID.
   * 
   * @param rev_id the review ID of the row to read
   * @throws SQLException
   */
  public void readReviewsById(int rev_id) throws SQLException {
    ResultSet rs = null;
    String sql= "{? = call read_package.read_reviewById(?)}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.setInt(2, rev_id);
      stmt.execute();
      rs=(ResultSet) stmt.getObject(1);
      while(rs.next()) {
        System.out.println("\nReview ID: " + rs.getInt("review_id")+"\nProduct Review: "+rs.getInt("product_review")+"\nReview Flag Number: "+rs.getInt("review_flag")+"\nReview Description: "+rs.getString("review_description")+"\nCustomer ID corresponding with this review: "+rs.getInt("customer_id")+"\nProduct ID corresponding with this review: "+rs.getInt("product_id"));
      }
    }catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * This reads a row of the Warehouses table by ID.
   * 
   * @param warehouse_id the warehouse ID of the row to read
   * @throws SQLException
   */
  public void readWarehousesById(int warehouse_id) throws SQLException {
    ResultSet rs = null;
    String sql= "{? = call read_package.read_warehouseById(?)}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.setInt(2, warehouse_id);
      stmt.execute();
      rs=(ResultSet) stmt.getObject(1);
      while(rs.next()) {
        System.out.println("\nWarehouse ID: "+rs.getInt("warehouse_id")+"\nWarehouse name: "+rs.getString("name")+"\nAddress: "+rs.getString("address"));
      }
    }catch(SQLException e) {
      e.printStackTrace();
    }
  }  
  /**
   * This reads a row of the WarehouseProducts table by product ID.
   * 
   * @param prod_id the product ID of the row to read
   * @throws SQLException
   */
  public void readWarehouseProductsByProdId(int prod_id) throws SQLException {
    ResultSet rs = null;
    String sql= "{? = call read_package.read_warehouseProdByPID(?)}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.setInt(2, prod_id);
      stmt.execute();
      rs=(ResultSet) stmt.getObject(1);
      while(rs.next()) {
        System.out.println("\nWarehouse ID: "+rs.getInt("warehouse_id")+"\nProduct ID: "+rs.getInt("product_id")+"\nProduct Quantity: "+rs.getInt("product_quantity"));
      }
    }catch(SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * This reads a row of the WarehouseProducts table by warehouse ID.
   * 
   * @param warehouse_id the warehouse ID of the row to read
   * @throws SQLException
   */
  public void readWarehouseProductsByWareId(int warehouse_id) throws SQLException {
    ResultSet rs = null;
    String sql= "{? = call read_package.read_warehouseProdByWID(?)}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.registerOutParameter(1, OracleTypes.CURSOR);
      stmt.setInt(2, warehouse_id);
      stmt.execute();
      rs=(ResultSet) stmt.getObject(1);
      while(rs.next()) {
        System.out.println("\nWarehouse ID: "+rs.getInt("warehouse_id")+"\nProduct ID: "+rs.getInt("product_id")+"\nProduct Quantity: "+rs.getInt("product_quantity"));
      }
    }catch(SQLException e) {
      e.printStackTrace();
    }
  }

  // INSERT METHODS

  /**
   * This inserts a new row to the Customer table.
   * 
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public void addCustomer() throws SQLException, ClassNotFoundException {
    System.out.println("Creating new customer...");
    
    System.out.println("Enter firstname: ");
    String fname = SCAN.nextLine();

    System.out.println("Enter lastname: ");
    String lname = SCAN.nextLine();

    System.out.println("Enter email: ");
    String email = SCAN.nextLine();

    System.out.println("Enter address: ");
    String address = SCAN.nextLine();

    Customers newCustomer = new Customers(fname, lname, email, address);
    newCustomer.addToDataBase(this.conn);
    System.out.println("\nCustomer added!\n");
  }

  /**
   * This inserts a new row to the Products table.
   * 
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public void addProduct() throws SQLException, ClassNotFoundException {
    System.out.println("Creating new product...");

    System.out.println("Enter product name: ");
    String name = SCAN.nextLine();

    System.out.println("Enter product category: ");
    String category = SCAN.nextLine();

    Products newProduct = new Products(name, category);
    newProduct.addToDataBase(this.conn);
    System.out.println("\nProduct added!\n");
  }

  /**
   * This inserts a new row to the Orders table.
   * 
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public void addOrder() throws SQLException, ClassNotFoundException {
    boolean badInput = true;
    System.out.println("Creating new order...");
    while (badInput) {
      try {
        ZoneId zonedId = ZoneId.of( "America/Montreal" );
        LocalDate date = LocalDate.now(zonedId);
        Date date_ordered = java.sql.Date.valueOf(date);

        System.out.println("Enter store ordered on: ");
        String store_ordered = SCAN.nextLine();

        System.out.println("Enter your product's ID: ");
        int product_id = SCAN.nextInt();

        System.out.println("Enter warehouse ID of the product: ");
        int warehouse_id = SCAN.nextInt();
        
        System.out.println("Enter order quantity: ");
        int order_quantity = SCAN.nextInt();

        System.out.println("Enter your ID (Customer ID): ");
        int customer_id = SCAN.nextInt();

        System.out.println("Enter order price: ");
        double order_price = SCAN.nextDouble();
        SCAN.nextLine();

        String totStock="{?=call read_package.totalInv(?)}";
        int totalStock=0;

        try(CallableStatement totalStockProd=conn.prepareCall(totStock)) {
          totalStockProd.registerOutParameter(1, Types.INTEGER);
          totalStockProd.setInt(2,product_id);
          totalStockProd.execute();
          totalStock=totalStockProd.getInt(1);
        } catch( SQLException e) {
          e.printStackTrace();
        }

        if (order_quantity <= totalStock) {
          String sql = "{? = call insert_package.get_product_quantity(?, ?)}";

          CallableStatement getProductQuantity = this.conn.prepareCall(sql);
          getProductQuantity.registerOutParameter(1, Types.NUMERIC);
          getProductQuantity.setInt(2, warehouse_id);
          getProductQuantity.setInt(3, product_id);
          getProductQuantity.execute();
          int currentQuantity = getProductQuantity.getInt(1);
          updateWarehouseProducts(warehouse_id, product_id, (currentQuantity - order_quantity));

          Orders newOrder = new Orders(date_ordered, order_quantity, order_price, store_ordered, customer_id, product_id);
          newOrder.addToDataBase(this.conn);   
          System.out.println("\nOrder added!\n");
          badInput = false;
        }
        else {
          System.out.println("Product not found or not enough stocks! Cancelling order...\n");
          badInput = false;
        }
      }
      catch (SQLException error) {
        System.out.println("Data error! Make sure your product is in the warehouse entered! Cancelling order...");
        badInput = false;
      }
      catch (InputMismatchException ime) {
        System.out.println("Error on entered input! Cancelling order...\n");
        SCAN.nextLine();
        badInput = false;
      }
    }
  }

  /**
   * This inserts a new row to the Reviews table.
   * 
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public void addReview() throws SQLException, ClassNotFoundException { 
    boolean badInput = true;
    System.out.println("Creating new review...");
    
    while(badInput) {
      int product_review = 0;
      int review_flag = 0;
      String review_description = "";
      int customer_id = 0;
      int product_id = 0;
      String unnecessaryInput = "";
      try {
        boolean valid = false;
        while (!valid) {
          System.out.println("Enter rating (out of 5): ");
          product_review = SCAN.nextInt();
          unnecessaryInput = SCAN.nextLine();          
          valid = true;

          if (product_review > 5 || product_review < 0 || unnecessaryInput != "") {
            System.out.println("Error! Invalid input. Please try again.");
            valid = false;
          }
          unnecessaryInput = "";
        }

        System.out.println("Enter review comment: ");
        review_description = SCAN.nextLine();

        valid = false;
        while (!valid) {
          System.out.println("Enter your customer ID: ");
          customer_id = SCAN.nextInt();
          unnecessaryInput = SCAN.nextLine();          
          valid = true;

          if (unnecessaryInput != "" || customer_id < 1) {
            System.out.println("Error! Invalid input. Please try again.");
            valid = false;
          }
          unnecessaryInput = "";
        }

        valid = false;
        while (!valid) {
          System.out.println("Enter ID of product being reviewed: ");
          product_id = SCAN.nextInt();
          unnecessaryInput = SCAN.nextLine();          
          valid = true;

          if (unnecessaryInput != "" || product_id < 1) {
            System.out.println("Error! Invalid input. Please try again.");
            valid = false;
          }
          unnecessaryInput = "";
        }
        Review newReview = new Review(product_review, review_flag, review_description, customer_id, product_id);
        newReview.addToDataBase(this.conn);
    
        badInput = false;
        System.out.println("\nReview added!\n");
      }
      catch (InputMismatchException ime){
        System.out.println("Error on entered input! Cancelling review...\n");
        SCAN.nextLine();
        badInput = false;
      }
      catch (SQLIntegrityConstraintViolationException error) {
        System.out.println("Could not find product or customer! Cancelling review...\n");
        badInput = false;
      }
    }
  }

  /**
   * This inserts a new row to the Warehouse table.
   * 
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public void addWarehouse() throws SQLException, ClassNotFoundException {
    System.out.println("Creating new warehouse...");

    System.out.println("Enter warehouse name: ");
    String name = SCAN.nextLine();

    System.out.println("Enter warehouse address: ");
    String address = SCAN.nextLine();

    Warehouse newWarehouse = new Warehouse(name, address);
    newWarehouse.addToDataBase(this.conn);
    System.out.println("\nWarehouse added!\n");
  }

  /**
   * This inserts a new row to the WarehouseProducts table.
   * 
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public void addWarehouseProducts() throws SQLException, ClassNotFoundException {
    try {
      int product_id = 0;
      int warehouse_id = 0;
      int product_quantity = 0;
      System.out.println("Adding quantity of products to a warehouse...");

      boolean badInput = true;

      while (badInput) {
        System.out.println("Enter Product ID: ");
        product_id = SCAN.nextInt();

        System.out.println("Enter Product's Warehouse's ID: ");
        warehouse_id = SCAN.nextInt();

        System.out.println("Enter quantity: ");
        product_quantity = SCAN.nextInt();
        SCAN.nextLine();
        badInput = false;

        if (product_id <= 0 || warehouse_id <= 0 || product_quantity <= 0) {
          System.out.println("Invalid ID input entered! Please try again...\n");
          badInput = true;
        }
      }

      WarehouseProducts newWarehouseProduct = new WarehouseProducts(warehouse_id, product_id, product_quantity);
      newWarehouseProduct.addToDataBase(this.conn);
      System.out.println("\nProduct to warehouse added!\n");
    }
    catch (InputMismatchException ime){
      System.out.println("Error on entered input! Cancelling adding product to warehouse...\n");
      SCAN.nextLine();
    }
    catch (SQLIntegrityConstraintViolationException error) {
      System.out.println("Could not find product or warehouse! Cancelling adding product to warehouse...\n");
    }
  } 

  // DELETE METHODS

  /**
   * This deletes a row in the Customer table by ID.
   * 
   * @param customer_id the customer ID of the row to delete
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public void deleteCustomer(int customer_id) throws SQLException, ClassNotFoundException {
    System.out.println("Deleting Customer...");
    String sql="{call delete_package.delete_customer(?)}";
    try (CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.setInt(1,customer_id);
      stmt.execute();
      System.out.println("Customer Deleted.");
    }catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * This deletes a row in the Reviews table by ID.
   * 
   * @param review_id the review ID of the row to delete
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public void deleteReview (int review_id) throws SQLException, ClassNotFoundException {
    System.out.println("Deleting Review...");
    String sql="{call delete_package.delete_review(?)}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.setInt(1,review_id);
      stmt.execute();
      System.out.println("Review Deleted");
    }catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * This deletes a row in the Products table by ID.
   * 
   * @param product_id the product ID of the row to delete
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public void deleteProduct (int product_id) throws SQLException, ClassNotFoundException {
    System.out.println("Deleting product...");
    String sql="{call delete_package.delete_product(?)}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.setInt(1, product_id);
      stmt.execute();
      System.out.println("Product Deleted");
    }catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * This deletes a row in the Order table by ID.
   * 
   * @param order_id the order ID of the row to delete
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public void deleteOrder (int order_id) throws SQLException, ClassNotFoundException {
    System.out.println("Deleting Order...");
    String sql="{call delete_package.delete_order(?)}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.setInt(1,order_id);
      stmt.execute();
      System.out.println("Order Deleted");
    }catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * This deletes a row in the Warehouse table by ID.
   * 
   * @param warehouse_id the warehouse ID of the row to delete
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public void deleteWarehouse(int warehouse_id) throws SQLException,ClassNotFoundException {
    System.out.println("Deleting Warehouse...");
    String sql="{call delete_package.delete_warehouse(?)}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.setInt(1,warehouse_id);
      stmt.execute();
      System.out.println("Warehouse Deleted");
    }catch (SQLException e) {
      e.printStackTrace();
    }
  }

  //UPDATE METHODS

  /**
   * This updates a row in the Customers table.
   * 
   * @param cus_id  the customer ID
   * @param email   the updated email
   * @param address the updated address
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public void updateCustomers(int cus_id,String email,String address) throws SQLException, ClassNotFoundException {
    System.out.println("Updating Customer...");
    String sql="{call update_package.update_customers(?,?,?)}";
    try(CallableStatement stmt = conn.prepareCall(sql)) {
      stmt.setInt(1,cus_id);
      stmt.setString(2, email);
      stmt.setString(3, address);
      stmt.execute();
      System.out.println("Customer Email and Address Changed");
    }catch (SQLException e) {
      e.printStackTrace();
    }
  }

  /**
   * This updates a row in the Review table (Its review flag).
   * 
   * @param revId the review ID
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public void updateReview(int revId) throws SQLException, ClassNotFoundException {
    System.out.println("Updating Review");
      String sql="{call update_package.update_review(?)}";
      try (CallableStatement stmt=conn.prepareCall(sql)) {
        stmt.setInt(1,revId);
        stmt.execute();
        System.out.println("Review Flag Updated");
      }catch (SQLException e) {
        e.printStackTrace();
      }
  }

  /**
   * This updates a row in the WarehouseProducts table.
   * 
   * @param wh_Id     the warehouse ID
   * @param prod_id   the product ID
   * @param quantity  the updated quantity of products in that warehouse
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public void updateWarehouseProducts(int wh_Id,int prod_id,int quantity) throws SQLException, ClassNotFoundException {
    System.out.println("Updating Warehouse Product"); 
    String sql="{call update_package.update_warehouseProducts(?,?,?)}";
    try(CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.setInt(1,wh_Id);
      stmt.setInt(2,prod_id);
      stmt.setInt(3,quantity);
      stmt.execute();
    }catch (SQLException e){
      e.printStackTrace();
      System.out.println("Error!");
    }
  }

  /**
   * This updates the Warehouse table.
   * 
   * @param wh_id   the warehouse ID
   * @param address the updated address
   * @throws SQLException
   * @throws ClassNotFoundException
   */
  public void updateWarehouse(int wh_id, String address) throws SQLException, ClassNotFoundException{
    System.out.println("Updating Warehouse");
    String sql="{call update_package.update_warehouse(?,?)}";
    try (CallableStatement stmt=conn.prepareCall(sql)) {
      stmt.setInt(1,wh_id);
      stmt.setString(2,address);
      stmt.execute(); 
    }catch(SQLException e) {
      e.printStackTrace();
    } 
  }
}
