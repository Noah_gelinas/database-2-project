package databaseproject;

import java.util.Map;
import java.sql.SQLException;
import java.sql.SQLData;
import java.sql.SQLOutput;
import java.sql.SQLInput;
import java.sql.Connection;
import java.sql.CallableStatement;

/**
 * A Customers class that represents a Customers table that includes its constructor, 
 * its fields' getters and setters, and overrides of SQLData to allow object serialization. 
 */
public class Products implements SQLData {
  private String name;
  private String category;

  public Products(String name, String category) {
    this.name = name;
    this.category = category;
  }

  public String getName() {
    return this.name;
  }

  public String getCategory() {
    return this.category;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setCategory(String category) {
    this.category = category;
  }

  @Override
  public String getSQLTypeName() throws SQLException {
    return "PRODUCTS_TYPE";
  }

  @Override
  public void writeSQL(SQLOutput stream) throws SQLException {
    stream.writeString(getName());
    stream.writeString(getCategory());
  }

  @Override
  public void readSQL(SQLInput stream, String typeName) throws SQLException {
    setName(stream.readString());
    setCategory(stream.readString());
  }

  public void addToDataBase(Connection conn) throws SQLException, ClassNotFoundException {
    Map<String, Class<?>> map = conn.getTypeMap();
    conn.setTypeMap(map);
    map.put("PRODUCTS_TYPE", Class.forName("databaseproject.Products"));
    Products product = new Products(this.name, this.category);
    String sql = "{call insert_package.add_product(?)}";
    try( CallableStatement stmt = 
      conn.prepareCall(sql)
    ) {
        stmt.setObject(1, product);
        stmt.execute();
      }
  }

  @Override
  public String toString() {
    return ("\nName: " + this.name + "\nCategory: " + this.category + "\n");
  }
}


