package databaseproject;

import java.util.Map;
import java.sql.SQLException;
import java.sql.SQLData;
import java.sql.SQLOutput;
import java.sql.SQLInput;
import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.Date;

/**
 * An Orders class that represents an Orders table that includes its constructor, 
 * its fields' getters and setters, and overrides of SQLData to allow object serialization. 
 */
public class Orders implements SQLData {
  private Date date_ordered;
  private int order_quantity;
  private double order_price;
  private String store_ordered;
  private int customer_id;
  private int product_id;

  public Orders(Date date_ordered, int order_quantity, double order_price, String store_ordered, int customer_id, int product_id) {
    this.date_ordered = date_ordered;
    this.order_quantity = order_quantity;
    this.order_price = order_price;
    this.store_ordered = store_ordered;
    this.customer_id = customer_id;
    this.product_id = product_id;
  }

  public Date getDate_Ordered() {
    return this.date_ordered;
  }

  public int getOrder_Quantity() {
    return this.order_quantity;
  }

  public double getOrder_Price() {
    return this.order_price;
  }

  public String getStoreOrdered() {
    return this.store_ordered;
  }

  public int getCustomerId () {
    return this.customer_id;
  }

  public int getProductId () {
    return this.product_id;
  }

  public void setDate_Ordered(Date date) {
    this.date_ordered = date;
  }

  public void setOrder_Quantity(int order_quantity) {
    this.order_quantity = order_quantity;
  }

  public void setOrder_Price(double order_price) {
    this.order_price = order_price;
  }

  public void setStoreOrdered(String store_ordered) {
    this.store_ordered = store_ordered;
  }

  public void setCustomerID(int customer_id) {
    this.customer_id = customer_id;
  }

  public void setProductID(int product_id) {
    this.product_id = product_id;
  }

  @Override
  public String getSQLTypeName() throws SQLException {
    return "ORDERS_TYPE";
  }

  @Override
  public void writeSQL(SQLOutput stream) throws SQLException {
    stream.writeDate(getDate_Ordered());
    stream.writeInt(getOrder_Quantity());
    stream.writeDouble(getOrder_Price());
    stream.writeString(getStoreOrdered());
    stream.writeInt(getCustomerId());
    stream.writeInt(getProductId());
  }

  @Override
  public void readSQL(SQLInput stream, String typeName) throws SQLException {
    setDate_Ordered(stream.readDate());
    setOrder_Quantity(stream.readInt());
    setOrder_Price(stream.readDouble());
    setStoreOrdered(stream.readString());
    setCustomerID(stream.readInt());
    setProductID(stream.readInt());
  }

  public void addToDataBase(Connection conn) throws SQLException, ClassNotFoundException {
    Map<String, Class<?>> map = conn.getTypeMap();
    conn.setTypeMap(map);
    map.put("ORDERS_TYPE", Class.forName("databaseproject.Orders"));
    Orders order = new Orders(this.date_ordered, this.order_quantity, this.order_price, this.store_ordered, this.customer_id, this.product_id);
    String  sql = "{call insert_package.add_order(?)}";
    try(CallableStatement stmt =
      conn.prepareCall(sql)
    ) {
        stmt.setObject(1, order);
        stmt.execute();
      }
  }

  @Override
  public String toString() {
    return ("\nDate Ordered: " + this.date_ordered + "\nOrder Quantity: " + this.order_quantity + "\nOrder Price: $" + this.order_price + "\nStore Ordered: " + this.store_ordered + "\nCustomer ID: " + this.customer_id + "\nProduct ID: " + this.product_id + "\n");
  }
}


