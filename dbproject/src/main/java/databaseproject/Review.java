package databaseproject;

import java.util.Map;
import java.sql.SQLException;
import java.sql.SQLData;
import java.sql.SQLOutput;
import java.sql.SQLInput;
import java.sql.Connection;
import java.sql.CallableStatement;

/**
 * A Review class that represents a Reviews table that includes its constructor, 
 * its fields' getters and setters, and overrides of SQLData to allow object serialization. 
 */
public class Review implements SQLData {
  private int product_review;
  private int review_flag;
  private String review_description;
  private int customer_id;
  private int product_id;

  public Review(int product_review, int review_flag, String review_description, int customer_id, int product_id) {
    this.product_review = product_review;
    this.review_flag = review_flag;
    this.review_description = review_description;
    this.customer_id = customer_id;
    this.product_id = product_id;
  } 

  public int getProduct_Review() {
    return this.product_review;
  }

  public int getReviewFlag() {
    return this.review_flag;
  }

  public String getReviewDescription() {
    return this.review_description;
  }

  public int getCustomerId() {
    return this.customer_id;
  }

  public int getProductId() {
    return this.product_id;
  }

  public void setProduct_Review(int product_review) {
    this.product_review = product_review;
  }

  public void setReviewFlag(int review_flag) {
    this.review_flag = review_flag;
  }

  public void setReviewDescription(String review_description) {
    this.review_description = review_description;
  }

  public void setCustomerID(int customer_id) {
    this.customer_id = customer_id;
  }

  public void setProductId(int product_id) {
    this.product_id = product_id;
  }

  @Override
  public String getSQLTypeName() throws SQLException {
    return "REVIEWS_TYPE";
  }

  @Override
  public void writeSQL(SQLOutput stream) throws SQLException {
    stream.writeInt(getProduct_Review());
    stream.writeInt(getReviewFlag());
    stream.writeString(getReviewDescription());
    stream.writeInt(getCustomerId());
    stream.writeInt(getProductId());
  }

  @Override
  public void readSQL(SQLInput stream, String typeName) throws SQLException {
    setProduct_Review(stream.readInt());
    setReviewFlag(stream.readInt());
    setReviewDescription(stream.readString());
    setCustomerID(stream.readInt());
    setProductId(stream.readInt());
  }

  public void addToDataBase(Connection conn) throws SQLException, ClassNotFoundException {
    Map<String, Class<?>> map = conn.getTypeMap();
    conn.setTypeMap(map);
    map.put("REVIEWS_TYPE", Class.forName("databaseproject.Review"));
    Review review = new Review(this.product_review, this.review_flag, this.review_description, this.customer_id, this.product_id);
    String sql = "{call insert_package.add_review(?)}";
    try(CallableStatement stmt = conn.prepareCall(sql)
    ) {
        stmt.setObject(1, review);
        stmt.execute();
      }     
  }

  @Override
  public String toString() {
    return ("\nProduct Review: " + this.product_review + "\nReview Flag: " + this.review_flag +"\nReview Description: " + this.review_description + "\nCustomer ID: " + this.customer_id + "\nProduct ID: " + this.product_id + "\n");
  }
}
