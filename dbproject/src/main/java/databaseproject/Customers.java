package databaseproject;

import java.util.Map;
import java.sql.SQLException;
import java.sql.SQLData;
import java.sql.SQLOutput;
import java.sql.SQLInput;
import java.sql.Connection;
import java.sql.CallableStatement;

/**
 * A Customers class that represents a Customers table that includes its constructor, 
 * its fields' getters and setters, and overrides of SQLData to allow object serialization. 
 */
public class Customers implements SQLData {
  private String firstname;
  private String lastname;
  private String email;
  private String address;

  public Customers(String firstname, String lastname, String email, String address) {
    this.firstname = firstname;
    this.lastname = lastname;
    this.email = email;
    this.address = address;
  }

  public String getFirstName () {
    return this.firstname;
  }
  public String getLastName() {
    return this.lastname;
  }

  public String getEmail() {
    return this.email;
  }

  public String getAddress() {
    return this.address;
  }

  public void setFirstName(String firstname) {
    this.firstname = firstname;
  }

  public void setLastName(String lastname) {
    this.lastname = lastname;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  @Override
  public String getSQLTypeName() throws SQLException {
    return "CUSTOMERS_TYPE";
  }

  @Override
  public void writeSQL(SQLOutput stream) throws SQLException {
    stream.writeString(getFirstName());
    stream.writeString(getLastName());
    stream.writeString(getEmail());
    stream.writeString(getAddress());
  }

  @Override
  public void readSQL(SQLInput stream, String typeName) throws SQLException {
    setFirstName(stream.readString());
    setLastName(stream.readString());
    setEmail(stream.readString());
    setAddress(stream.readString());
  }

  public void addToDataBase(Connection conn) throws SQLException, ClassNotFoundException {
    Map<String, Class<?>> map = conn.getTypeMap();
    conn.setTypeMap(map);
    map.put("CUSTOMERS_TYPE", Class.forName("databaseproject.Customers"));
    Customers customer = new Customers(this.firstname, this.lastname, this.email, this.address);
    String sql = "{call insert_package.add_customer(?)}";
    try( CallableStatement stmt =
      conn.prepareCall(sql)
    ) {
        stmt.setObject(1, customer);
        stmt.execute();
      }
  }

  @Override 
  public String toString() {
    return ("\nFirstname: " + this.firstname + "\nLastname: " + this.lastname + "\nEmail: " + this.email + "\nAddress: " + this.address + "\n");
  }
}


