/* package for inserting into database*/
CREATE OR REPLACE PACKAGE insert_package AS
    PROCEDURE add_customer(VCustomers IN CUSTOMERS_TYPE);
    PROCEDURE add_product(VProducts IN PRODUCTS_TYPE);
    PROCEDURE add_order(VOrders IN ORDERS_TYPE);
    PROCEDURE add_review(VReviews IN REVIEWS_TYPE);
    PROCEDURE add_warehouse(VWarehouses IN WAREHOUSES_TYPE);
    PROCEDURE add_warehouse_products(VWarehouse_Products IN WAREHOUSE_PRODUCTS_TYPE);
    FUNCTION get_product_quantity(awarehouse_id IN Warehouse_Products.warehouse_id%TYPE, aproduct_id IN Warehouse_Products.product_id%TYPE) RETURN NUMBER;
END insert_package;
/
CREATE OR REPLACE PACKAGE BODY insert_package AS    
    /*procedure to add customer to database*/
    PROCEDURE add_customer(VCustomers IN CUSTOMERS_TYPE)
    IS
    BEGIN
        INSERT INTO SS_Customers(firstname, lastname, email, address)
            VALUES (VCustomers.firstname, VCustomers.lastname, VCustomers.email, VCustomers.address);
    END;
    
    /*procedure to add products to database*/
    PROCEDURE add_product(VProducts IN PRODUCTS_TYPE)
    IS
    BEGIN
        INSERT INTO SS_Products(name, category)
            VALUES (VProducts.name, VProducts.category);
    END;
    
    /*procedure to add orders to database*/
    PROCEDURE add_order(VOrders IN ORDERS_TYPE)
    IS
    BEGIN
        INSERT INTO SS_Orders(date_ordered, order_quantity, order_price, store_ordered, customer_id, product_id)
            VALUES (VOrders.date_ordered, VOrders.order_quantity, VOrders.order_price, VOrders.store_ordered, VOrders.customer_id, VOrders.product_id);
    END;
    
    /*procedure to add reviews to database*/
    PROCEDURE add_review(VReviews IN REVIEWS_TYPE)
    IS
    BEGIN
        INSERT INTO SS_Reviews(product_review, review_flag, review_description, customer_id, product_id)
            VALUES(VReviews.product_review, VReviews.review_flag, VReviews.review_description, VReviews.customer_id, VReviews.product_id);
    END;
    
    /*procedure to add warehouses to database*/
    PROCEDURE add_warehouse(VWarehouses IN WAREHOUSES_TYPE)
    IS
    BEGIN
        INSERT INTO SS_Warehouses(name, address)
            VALUES(VWarehouses.name, VWarehouses.address);
    END;
    
    /*procedure to add warehouse_products to database*/
    PROCEDURE add_warehouse_products(VWarehouse_Products IN WAREHOUSE_PRODUCTS_TYPE)
    IS
    BEGIN
        INSERT INTO Warehouse_Products(warehouse_id, product_id, product_quantity)
            VALUES(VWarehouse_Products.warehouse_id, VWarehouse_Products.product_id, VWarehouse_Products.product_quantity);
    END;
    
    FUNCTION get_product_quantity(awarehouse_id IN Warehouse_Products.warehouse_id%TYPE, aproduct_id IN Warehouse_Products.product_id%TYPE) 
    RETURN NUMBER
    IS
        quantity    Warehouse_Products.product_quantity%TYPE;
    BEGIN
        SELECT product_quantity INTO quantity FROM Warehouse_Products 
        WHERE warehouse_id = awarehouse_id AND product_id = aproduct_id;
        RETURN quantity;
    END;
END insert_package;
/

/*Package to delete from database*/
CREATE OR REPLACE PACKAGE delete_package AS
    PROCEDURE delete_Customer(cus_id IN SS_Customers.customer_id%TYPE);
    PROCEDURE delete_product(prod_id IN SS_Products.product_id%TYPE);
    PROCEDURE delete_order(orderId IN SS_Orders.order_id%TYPE);
    PROCEDURE delete_review(revId IN SS_Reviews.review_id%TYPE);
    PROCEDURE delete_warehouse(whId IN SS_Warehouses.warehouse_id%TYPE);
END delete_package; 
/
CREATE OR REPLACE PACKAGE BODY delete_package AS 
    e_invalid_input EXCEPTION;
 
    /*Procedure to delete customers from database
    * cus_id represents user input customer id*/
    PROCEDURE delete_customer(cus_id IN SS_Customers.customer_id%TYPE) 
        IS
        BEGIN
            DELETE FROM SS_Reviews
            WHERE customer_id = cus_id;
            DELETE FROM SS_Orders 
            WHERE customer_id = cus_id;
            DELETE FROM SS_Customers 
            WHERE customer_id=cus_id;
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END; 
    
    /*Procedure to delete products from database
    * prod_id represents user input product id*/
    PROCEDURE delete_product(prod_id IN SS_Products.product_id%TYPE)
        IS
        BEGIN
            DELETE FROM SS_Reviews
            WHERE product_id=prod_id;
            DELETE FROM SS_Orders 
            WHERE product_id=prod_id;
            DELETE FROM Warehouse_Products 
            WHERE product_id=prod_id;
            DELETE FROM SS_Products 
            WHERE product_id=prod_id;
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('Category doesnt exist lil bro');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END; 
    
    /*Procedure to delete orders from database
    * orderId represents user input order id*/
    PROCEDURE delete_order(orderId IN SS_Orders.order_id%TYPE)
        IS
        BEGIN
            DELETE FROM SS_Orders
            WHERE order_id=orderId;
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END; 
    
    /*Procedure to delete reviews from database
    * revId represents user input review id*/
    PROCEDURE delete_review(revId IN SS_Reviews.review_id%TYPE)
        IS 
        BEGIN
            DELETE FROM SS_Reviews
            WHERE review_id=revId;
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;  
    
    /*Procedure to delete warehouses from database
    * whId represents user input warehouse id*/
    PROCEDURE delete_warehouse(whId IN SS_Warehouses.warehouse_id%TYPE)
        IS
        BEGIN
            DELETE FROM warehouse_products 
            WHERE warehouse_id=whId;
            DELETE FROM SS_Warehouses
            WHERE warehouse_id=whId;
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
            EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;  
END delete_package;
/

/*Package for updating the database*/
CREATE OR REPLACE PACKAGE update_package AS
    PROCEDURE update_customers(cus_id IN SS_Customers.customer_id%TYPE, email_change IN SS_Customers.email%TYPE, address_change IN SS_Customers.address%TYPE);
    PROCEDURE update_review(rev_id IN SS_Reviews.review_id%TYPE);
    PROCEDURE update_warehouseProducts(wh_id IN Warehouse_Products.warehouse_id%TYPE, prod_id IN Warehouse_Products.product_id%TYPE,quantity IN Warehouse_Products.product_quantity%TYPE);
    PROCEDURE update_warehouse(wh_id IN SS_Warehouses.warehouse_id%TYPE, new_address IN SS_Warehouses.address%TYPE);
END update_package;
/

CREATE OR REPLACE PACKAGE BODY update_package AS 
    e_invalid_input EXCEPTION;
    
    /*Procedure to update customers
    * cus_id represents user input customer id
    * email_change represents user input new email
    * address_change represents user input new address
    */
    PROCEDURE update_customers(cus_id IN SS_Customers.customer_id%TYPE, email_change IN SS_Customers.email%TYPE, address_change IN SS_Customers.address%TYPE)
        IS
        BEGIN
            UPDATE SS_Customers SET email = email_change WHERE customer_id=cus_id;
            UPDATE SS_Customers SET address = address_change WHERE customer_id=cus_id;
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /*Procedure to update reviews
    * rev_id represents user input review id
    * revFlag represents user input new flag number
    */
    PROCEDURE update_review(rev_id IN SS_Reviews.review_id%TYPE)
        IS 
        BEGIN
            UPDATE SS_Reviews SET review_flag = (SELECT review_flag+1 FROM SS_Reviews WHERE review_id = rev_id) WHERE review_id = rev_id;
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /*Procedure to update warehouse_products
    * wh_id represents user input warehouse id
    * prod_id represents user input product_id
    * quantity represents user input new quantity
    */
    PROCEDURE update_warehouseProducts(wh_id IN Warehouse_Products.warehouse_id%TYPE, prod_id IN Warehouse_Products.product_id%TYPE,quantity IN Warehouse_Products.product_quantity%TYPE)
        IS
        BEGIN
            UPDATE Warehouse_Products SET product_quantity = quantity WHERE warehouse_id=wh_id AND product_id=prod_id;
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /*Procedure to update warehouse
    * wh_id represents user input warehouse id
    * address_change represents user input new address
    */
    PROCEDURE update_warehouse(wh_id IN SS_Warehouses.warehouse_id%TYPE, new_address IN SS_Warehouses.address%TYPE)
        IS
        BEGIN
            UPDATE SS_Warehouses SET address = new_address WHERE warehouse_id = wh_id;
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
END update_package;       
/

/*Package for reading from database*/
CREATE OR REPLACE PACKAGE read_package AS
    TYPE table_cursor IS REF CURSOR;
    FUNCTION read_customers RETURN table_cursor;
    FUNCTION read_products RETURN table_cursor;
    FUNCTION read_orders RETURN table_cursor;
    FUNCTION read_reviews RETURN table_cursor;
    FUNCTION read_warehouses RETURN table_cursor;
    FUNCTION read_warehouseProducts RETURN table_cursor;
    FUNCTION read_auditOrders RETURN table_cursor;
    FUNCTION read_customersById (cus_id IN SS_Customers.customer_id%TYPE) RETURN table_cursor;
    FUNCTION read_auditById(aud_id IN SS_AuditOrders.audit_id%TYPE) RETURN table_cursor;
    FUNCTION read_ordersById(ord_id IN SS_Orders.order_id%TYPE) RETURN table_cursor;
    FUNCTION read_productById(prod_id IN SS_Products.product_id%TYPE) RETURN table_cursor;
    FUNCTION read_reviewById(rev_id IN SS_Reviews.review_id%TYPE) RETURN table_cursor;
    FUNCTION read_warehouseById(ware_id IN SS_WAREHOUSES.warehouse_id%TYPE) RETURN table_cursor;
    FUNCTION read_warehouseProdByPID (prod_id IN warehouse_products.product_id%TYPE) RETURN table_cursor;
    FUNCTION read_warehouseProdByWID (ware_id IN warehouse_products.warehouse_id%TYPE) RETURN table_cursor;
    FUNCTION countOrders(cus_id IN SS_Orders.customer_id%TYPE) RETURN NUMBER;
    FUNCTION avgReview (prod_id SS_Reviews.product_id%TYPE) RETURN NUMBER;
    FUNCTION totalInv(prod_id warehouse_products.product_id%TYPE) RETURN NUMBER;
    FUNCTION totalOrdered(prod_id SS_Orders.product_id%TYPE) RETURN NUMBER;
END read_package;
/
CREATE OR REPLACE PACKAGE BODY read_package AS 
    e_invalid_input EXCEPTION;
    
    /* Function to count number of orders per customer
    * Returns countId number of orders
    * cus_id represents user input customer id
    */
    FUNCTION countOrders(cus_id IN SS_Orders.customer_id%TYPE) RETURN NUMBER
        IS
            countId NUMBER(6);
        BEGIN
            SELECT COUNT(*) INTO countId FROM SS_Orders 
            WHERE customer_id=cus_id;
            RETURN countId;
            
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /* Function to read customers from database 
    * Returns customer_table Cursor which has all customer rows
    */
    FUNCTION read_customers RETURN table_cursor 
        IS
            customer_table table_cursor;
        BEGIN
            OPEN customer_table FOR 
            SELECT c.*
            FROM SS_Customers c;
            RETURN customer_table;
            
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /* Function to find avg review score for each product 
    * Returns avgRev NUMBER representing avg score
    * prod_id represents user input product id
    */
    FUNCTION avgReview (prod_id SS_Reviews.product_id%TYPE) RETURN NUMBER
        IS  
            avgRev NUMBER(2,1);
        BEGIN 
            SELECT AVG(product_review) INTO avgRev
            FROM SS_REVIEWS
            WHERE product_id=prod_id;
            RETURN avgRev;
            
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /* Function to find total stock of a product
    * Returns totalStock which is the total amount of a product
    * prod_id represents user input product id 
    */
    FUNCTION totalInv(prod_id warehouse_products.product_id%TYPE) RETURN NUMBER
        IS
            totalStock NUMBER(20);
        BEGIN
            SELECT SUM(product_quantity) INTO totalStock
            FROM warehouse_products
            WHERE product_id=prod_id;
            RETURN totalStock;
            
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /* Function to find how many times  product was ordered
    * Returns totalOrd NUMBER total times ordered
    * prod_id represents user input product id
    */
    FUNCTION totalOrdered(prod_id SS_Orders.product_id%TYPE) RETURN NUMBER
        IS
            totalOrd NUMBER(6);
        BEGIN
            SELECT COUNT(*) INTO totalOrd
            FROM SS_Orders
            WHERE product_id=prod_id;
            RETURN totalOrd;
            
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /* Function to read products from database 
    * Returns products_table Cursor which has all product rows
    */
    FUNCTION read_products RETURN table_cursor
        IS
            products_table table_cursor;
        BEGIN
            OPEN products_table FOR 
            SELECT *
            FROM SS_Products;
            RETURN products_table;
            
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /* Function to read orders from database 
    * Returns orders_table Cursor which has all orders rows
    */
    FUNCTION read_orders RETURN table_cursor
        IS
            orders_table table_cursor;
        BEGIN
            OPEN orders_table FOR 
            SELECT *
            FROM SS_Orders;
            RETURN orders_table;
            
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /* Function to read reviews from database 
    * Returns reviews_table Cursor which has all review rows
    */
    FUNCTION read_reviews RETURN table_cursor
        IS
            reviews_table table_cursor;
        BEGIN
            OPEN reviews_table FOR 
            SELECT *
            FROM SS_Reviews;
            RETURN reviews_table;
            
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /* Function to read warehouse from database 
    * Returns warehouse_table Cursor which has all warehouses rows
    */
    FUNCTION read_warehouses RETURN table_cursor
        IS
            warehouse_table table_cursor;
        BEGIN   
            OPEN warehouse_table FOR 
            SELECT *
            FROM SS_Warehouses;
            RETURN warehouse_table;
            
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /* Function to read warehouse products from database 
    * Returns warehouseProducts_table Cursor which has all warehouse products rows
    */
    FUNCTION read_warehouseProducts RETURN table_cursor
        IS
            warehouseProducts_table table_cursor;
        BEGIN
            OPEN warehouseProducts_table FOR 
            SELECT *
            FROM Warehouse_Products;
            RETURN warehouseProducts_table;
            
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /* Function to read audits from database 
    * Returns audit_table Cursor which has all audit order rows
    */
    FUNCTION read_auditOrders RETURN table_cursor
        IS
            audit_table table_cursor;
        BEGIN
            OPEN audit_table FOR
            SELECT *
            FROM SS_AuditOrders;
            RETURN audit_table;
            
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /* Function to read customers by id from database 
    * Returns cusById Cursor which has all customer rows for that id
    * cus_id represents user input customer id to search for
    */
    FUNCTION read_customersById (cus_id IN SS_Customers.customer_id%TYPE) RETURN table_cursor
        IS
            cusById table_cursor;
        BEGIN
            OPEN cusById FOR 
            SELECT * 
            FROM SS_Customers 
            WHERE customer_id=cus_id;
            RETURN cusById;
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
        
    /* Function to read audits by id from database 
    * Returns audById Cursor which has all audits rows for that id
    * aud_id represents user input audit id to search for
    */
    FUNCTION read_auditById (aud_id IN SS_AuditOrders.audit_id%TYPE) RETURN table_cursor
        IS
            audById table_cursor;
        BEGIN
            OPEN audById FOR
            SELECT * 
            FROM SS_AuditOrders
            WHERE audit_id = aud_id;
            RETURN audById;
            
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /* Function to read orders by id from database 
    * Returns ordById Cursor which has all orders rows for that id
    * ord_id represents user input orders id to search for
    */
    FUNCTION read_ordersById(ord_id IN SS_Orders.order_id%TYPE) RETURN table_cursor
        IS
            ordById table_cursor;
        BEGIN
            OPEN ordById FOR 
            SELECT * 
            FROM SS_Orders
            WHERE order_id=ord_id;
            RETURN ordById;
            
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
            
    /* Function to read products by id from database 
    * Returns prodById Cursor which has all products rows for that id
    * prod_id represents user input prod id to search for
    */
    FUNCTION read_productById(prod_id IN SS_Products.product_id%TYPE) RETURN table_cursor
        IS 
            prodById table_cursor;
        BEGIN
            OPEN prodById FOR
            SELECT * 
            FROM SS_Products
            WHERE product_id=prod_id;
            RETURN prodById;
            
            IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /* Function to read review by id from database 
    * Returns revById Cursor which has all review rows for that id
    * rev_id represents user input review id to search for
    */
    FUNCTION read_reviewById(rev_id IN SS_Reviews.review_id%TYPE) RETURN table_cursor
    IS
        revById table_cursor;
    BEGIN
        OPEN revById FOR 
        SELECT *
        FROM  SS_Reviews
        WHERE review_id=rev_id;
        RETURN revById;
        
        IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /* Function to read warehouse by id from database 
    * Returns wareById Cursor which has all warehouse rows for that id
    * ware_id represents user input warehouse id to search for
    */
    FUNCTION read_warehouseById(ware_id IN SS_WAREHOUSES.warehouse_id%TYPE) RETURN table_cursor
    IS
        wareById table_cursor;
    BEGIN
        OPEN wareById FOR
        SELECT * 
        FROM SS_WAREHOUSES
        WHERE warehouse_id=ware_id;
        RETURN wareById;
        
        IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /* Function to read warehouseProducts by product id from database 
    * Returns wpByPid Cursor which has all warehouse products rows for that product id
    * prod_id represents user input product id to search for
    */
    FUNCTION read_warehouseProdByPID (prod_id IN warehouse_products.product_id%TYPE) RETURN table_cursor
    IS
        wpByPid table_cursor;
    BEGIN
        OPEN wpByPid FOR
        SELECT *
        FROM warehouse_products
        WHERE product_id=prod_id;
        RETURN wpByPid;
        
        IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
    
    /* Function to read warehouseProducts by warehouse id from database 
    * Returns wpByWid Cursor which has all warehouse products rows for that warehouse id
    * ware_id represents user input warehouse id to search for
    */
    FUNCTION read_warehouseProdByWID (ware_id IN warehouse_products.warehouse_id%TYPE) RETURN table_cursor
    IS
        wpByWid table_cursor;
    BEGIN
        OPEN wpByWid FOR
        SELECT *
        FROM warehouse_products
        WHERE warehouse_id=ware_id;
        RETURN wpByWid;
        
        IF SQL%NOTFOUND THEN
                RAISE e_invalid_input;
            END IF;
       
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                dbms_output.put_line('No data found!');
            WHEN e_invalid_input THEN    
                dbms_output.put_line('input doesnt exist');
            WHEN OTHERS THEN
                dbms_output.put_line(SQLCODE);
    END;
END read_package;
/

    